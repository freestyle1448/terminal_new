package com.example.terminal.di;

import com.example.terminal.presentation.presenter.MainPresenter;
import com.example.terminal.presentation.presenter.ReceivePresenter;

import dagger.Component;

@Component
public interface AppComponent {
    void injectMainPresenter(MainPresenter presenter);

    void injectReceivePresenter(ReceivePresenter receivePresenter);
}
