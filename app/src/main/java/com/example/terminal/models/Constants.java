package com.example.terminal.models;

public final class Constants {
    public static final String APP_PREFERENCES = "terminalData";
    public static final String TERMINAL_LOGIN = "terminalLogin";
    public static final String TERMINAL_TOKEN = "terminalToken";
}
