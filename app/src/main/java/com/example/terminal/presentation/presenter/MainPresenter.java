package com.example.terminal.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.example.terminal.di.App;
import com.example.terminal.presentation.view.MainView;
import com.example.terminal.repositories.RestTemplate;
import com.google.gson.JsonObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {
    @Inject
    RestTemplate restTemplate;

    public MainPresenter() {
        App.getComponent().injectMainPresenter(this);
    }

    public void createInitRequest(String userToken, String amount) {
        NumberFormat nf_in = NumberFormat.getNumberInstance(Locale.GERMANY);
        double doubleAmount;
        try {
            doubleAmount = nf_in.parse(amount).doubleValue();

            restTemplate.createTransaction(userToken, (long) (doubleAmount * 100))
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.body() != null && response.body().get("order_id") != null) {
                                final String transactionId = response.body().get("order_id").getAsString();
                                if (transactionId != null) {
                                    getViewState().showQR(response.body().get("qr_payload").getAsString(), transactionId, doubleAmount);
                                }
                            } else {
                                getViewState().showError();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            getViewState().showError();
                        }
                    });
        } catch (ParseException e) {
            getViewState().showError();
        }
    }

    @Override
    protected void disconnect() {

    }
}
