package com.example.terminal.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.example.terminal.di.App;
import com.example.terminal.presentation.view.ReceiveView;
import com.example.terminal.repositories.RestTemplate;
import com.google.gson.JsonObject;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class ReceivePresenter extends BasePresenter<ReceiveView> {
    @Inject
    RestTemplate restTemplate;

    public ReceivePresenter() {
        App.getComponent().injectReceivePresenter(this);
    }

    public void checkTransaction(String userToken, String transactionId) {
        restTemplate.checkTransaction(userToken, transactionId)
                .enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        getViewState().error("");
                        if (response.body() != null)
                            if (response.body().get("status").getAsInt() == 2) {
                                getViewState().showDone();
                            }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        getViewState().error(t.getMessage());
                    }
                });
    }

    public void transactionDecline(String userToken, String transactionId) {
        restTemplate.declineTransaction(userToken, transactionId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                getViewState().clearPreferences();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    @Override
    protected void disconnect() {

    }
}
