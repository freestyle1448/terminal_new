package com.example.terminal.presentation.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.terminal.R;
import com.example.terminal.presentation.presenter.MainPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.terminal.models.Constants.APP_PREFERENCES;
import static com.example.terminal.models.Constants.TERMINAL_TOKEN;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    private static final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTIxYzJiNDk2MWVlMzA1NTk0MDI5YWQiLCJpYXQiOjE1NzkyNzE5NzUsImV4cCI6MTYxMDgwNzk3NX0.qGwad1ojiNkzoMEZxfC6qhqvsIQ8qtDFPnyd3uqKQuc";//dakota "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTIxYzJiNDk2MWVlMzA1NTk0MDI5YWQiLCJpYXQiOjE1NzkyNzE5NzUsImV4cCI6MTYxMDgwNzk3NX0.qGwad1ojiNkzoMEZxfC6qhqvsIQ8qtDFPnyd3uqKQuc";
    //zing "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTI4MmY0MTU2ZWIzMjBhZTQ0NGVlOGMiLCJpYXQiOjE1Nzk2OTU4MjYsImV4cCI6MTYxMTIzMTgyNn0.tcb4TguyZ6eLAx_wAJ5_QCpF5OYrtGkNK1AfuQ1e1Wo"
    @InjectPresenter
    MainPresenter mainPresenter;
    @BindView(R.id.amountEditText)
    EditText amountEditText;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.imageButtonCreate)
    ImageButton imageButtonCreate;
    @BindView(R.id.imageButtonRemove)
    ImageButton imageButtonRemove;
    @BindView(R.id.imageButtonClear)
    ImageButton imageButtonClear;
    @BindView(R.id.buttonCreate)
    Button buttonCreate;
    @BindView(R.id.buttonRemove)
    Button buttonRemove;
    @BindView(R.id.buttonClear)
    Button buttonClear;
    @BindView(R.id.button1)
    Button button1;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.button6)
    Button button6;
    @BindView(R.id.button7)
    Button button7;
    @BindView(R.id.button8)
    Button button8;
    @BindView(R.id.button9)
    Button button9;
    @BindView(R.id.buttonComma)
    Button buttonComma;
    @BindView(R.id.button0)
    Button button0;
    @BindView(R.id.button00)
    Button button00;
    private String userToken;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT < 21) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ButterKnife.bind(this);

        SharedPreferences mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = mSettings.edit();
        editor1.putString(TERMINAL_TOKEN, TOKEN);
        editor1.apply();

        SharedPreferences sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");
    }

    public void revertViewItemsState(boolean isState) {
        if (Build.VERSION.SDK_INT < 21) {
            imageButtonCreate.setVisibility(isState ? View.INVISIBLE : View.VISIBLE);
            imageButtonCreate.setClickable(!isState);
        } else {
            buttonCreate.setVisibility(isState ? View.INVISIBLE : View.VISIBLE);
            buttonCreate.setClickable(!isState);
        }
        progressBar.setVisibility(isState ? View.VISIBLE : View.INVISIBLE);
        button1.setClickable(!isState);
        button2.setClickable(!isState);
        button3.setClickable(!isState);
        button4.setClickable(!isState);
        button5.setClickable(!isState);
        button6.setClickable(!isState);
        button7.setClickable(!isState);
        button8.setClickable(!isState);
        button9.setClickable(!isState);
        buttonComma.setClickable(!isState);
        button0.setClickable(!isState);
        button00.setClickable(!isState);
        imageButtonClear.setClickable(!isState);
        imageButtonRemove.setClickable(!isState);
        buttonClear.setClickable(!isState);
        buttonRemove.setClickable(!isState);
    }

    @OnClick({R.id.imageButtonCreate, R.id.buttonCreate})
    public void onCreateClicked() {
        revertViewItemsState(true);
        Editable amountEditTextText = amountEditText.getText();
        if (amountEditTextText.length() > 0 && !amountEditTextText.toString().startsWith("0")) {
            final String amount = amountEditTextText.toString();
            mainPresenter.createInitRequest(userToken, amount);
        } else {
            amountEditText.requestFocus();
            amountEditText.setError("Введите сумму!");
            revertViewItemsState(false);
        }
    }

    @OnClick({R.id.button1, R.id.button2, R.id.button3,
            R.id.button4, R.id.button5, R.id.button6,
            R.id.button7, R.id.button8, R.id.button9,
            R.id.buttonComma, R.id.button0, R.id.button00})
    public void onNumPadClicked(Button button) {
        amountEditText.setText(amountEditText.getText().append(button.getText()));
    }

    @OnClick({R.id.imageButtonClear, R.id.imageButtonRemove})
    public void onClearImageButtonsClicked(ImageButton button) {
        switch (button.getId()) {
            case R.id.imageButtonClear:
                amountEditText.setText("");
                break;
            case R.id.imageButtonRemove:
                if (amountEditText.getText().length() > 0)
                    amountEditText.setText(
                            amountEditText.getText().subSequence(0, amountEditText.getText().length() - 1));
                break;
        }
    }

    @OnClick({R.id.buttonClear, R.id.buttonRemove})
    public void onClearButtonsClicked(Button button) {
        switch (button.getId()) {
            case R.id.buttonClear:
                amountEditText.setText("");
                break;
            case R.id.buttonRemove:
                if (amountEditText.getText().length() > 0)
                    amountEditText.setText(
                            amountEditText.getText().subSequence(0, amountEditText.getText().length() - 1));
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        revertViewItemsState(false);
    }

    @Override
    public void showQR(String qrURL, String transactionId, Double amount) {
        Intent intent = new Intent(MainActivity.this, ReceiveActivity.class);
        intent.putExtra("psbQR", qrURL);
        intent.putExtra("transactionId", transactionId);
        intent.putExtra("amount", amount);
        startActivity(intent);

        revertViewItemsState(false);
        amountEditText.setText("");
    }

    @Override
    public void showError() {
        Toast.makeText(MainActivity.this, "Произошла ошибка, повторите запрос позже.\nОшибка - "
                + "Не удалось создать транзакцию!", Toast.LENGTH_LONG).show();
        revertViewItemsState(false);
    }
}
