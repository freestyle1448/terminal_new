package com.example.terminal.presentation.view;


import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.terminal.R;
import com.example.terminal.presentation.presenter.ReceivePresenter;
import com.google.zxing.WriterException;

import java.util.Timer;
import java.util.TimerTask;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.terminal.models.Constants.APP_PREFERENCES;
import static com.example.terminal.models.Constants.TERMINAL_TOKEN;

public class ReceiveActivity extends MvpAppCompatActivity implements ReceiveView {
    final Timer timer = new Timer();
    @InjectPresenter
    ReceivePresenter receivePresenter;
    SharedPreferences sharedPreferences;
    @BindView(R.id.doneText)
    TextView doneText;
    @BindView(R.id.doneView)
    ImageView iconDone;
    @BindView(R.id.doneButton)
    Button doneButton;
    @BindView(R.id.buttonDecline)
    Button buttonDecline;
    @BindView(R.id.qrView)
    ImageView qrView;
    private String userToken;
    private boolean isCheckSend = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_layout);

        if (Build.VERSION.SDK_INT < 21) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ButterKnife.bind(this);

        sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("psbQR", getIntent().getStringExtra("psbQR"));
        editor.apply();
        final String transactionId = getIntent().getStringExtra("transactionId");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!isCheckSend) {
                    isCheckSend = true;
                    receivePresenter.checkTransaction(userToken, transactionId);
                }
            }
        }, 0, 500);

        QRGEncoder qrgEncoder1 = new QRGEncoder(getIntent().getStringExtra("psbQR"), null, QRGContents.Type.TEXT, 950);

        Bitmap bitmap1;
        try {
            bitmap1 = qrgEncoder1.encodeAsBitmap();
            qrView.setImageBitmap(bitmap1);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.doneButton)
    public void onDoneClick() {
        timer.cancel();
        timer.purge();

        ReceiveActivity.this.finish();
    }

    @OnClick(R.id.buttonDecline)
    public void onDeclineClick() {
        timer.cancel();
        timer.purge();

        receivePresenter.transactionDecline(userToken, getIntent().getStringExtra("transactionId"));
        ReceiveActivity.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("psbQR", "");
        editor.apply();
    }

    @Override
    public void showDone() {
        if (Build.VERSION.SDK_INT < 21) {
            runOnUiThread(this::getDoneScreen);
        } else {
            getDoneScreen();
        }
    }

    private void getDoneScreen() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        qrView.setVisibility(View.INVISIBLE);
        buttonDecline.setVisibility(View.INVISIBLE);

        doneText.setVisibility(View.VISIBLE);
        doneButton.setVisibility(View.VISIBLE);
        iconDone.setVisibility(View.VISIBLE);
    }

    @Override
    public void error(String error) {
        isCheckSend = false;
        System.err.println(error);
    }

    @Override
    public void clearPreferences() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("psbQR", "");
        editor.apply();
    }
}
