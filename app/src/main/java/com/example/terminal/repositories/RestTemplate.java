package com.example.terminal.repositories;

import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestTemplate {
    private final String BASE_URL = "https://merchant.zingpos.ru/";
    private final OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .build();
    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private final ServerApi serverApi = retrofit.create(ServerApi.class);

    @Inject
    public RestTemplate() {
    }

    public Call<JsonObject> createTransaction(String token, Long amount) {
        Call call;

        if (Build.VERSION.SDK_INT < 21) {
            call = new Call() {
                @Override
                public Response execute() throws IOException {
                    URL url = new URL(BASE_URL + "api/user/transaction/create");
                    HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
                    c.setRequestMethod("POST");
                    c.setReadTimeout(20000);
                    c.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    c.setRequestProperty("Token", token);
                    c.setDoOutput(true);

                    String request = "amount=" + amount;
                    try (OutputStream os = c.getOutputStream()) {
                        byte[] input = request.getBytes(StandardCharsets.UTF_8);
                        os.write(input, 0, input.length);
                    }

                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(c.getInputStream(), StandardCharsets.UTF_8))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }

                        return Response.success(new Gson().fromJson(response.toString(), JsonObject.class));
                    }
                }

                @Override
                public void enqueue(Callback callback) {
                    try {
                        callback.onResponse(this, execute());
                    } catch (IOException e) {
                        e.printStackTrace();
                        callback.onFailure(this, e);
                    }
                }

                @Override
                public boolean isExecuted() {
                    return false;
                }

                @Override
                public void cancel() {

                }

                @Override
                public boolean isCanceled() {
                    return false;
                }

                @Override
                public Call clone() {
                    return null;
                }

                @Override
                public Request request() {
                    return null;
                }
            };
        } else {
            call = serverApi.createTransaction(token,
                    amount);
        }

        return call;
    }

    public Call<JsonObject> checkTransaction(String token, String transactionId) {
        Call call;
        if (Build.VERSION.SDK_INT < 21) {
            call = new Call() {
                @Override
                public Response execute() throws IOException {
                    URL url = new URL(BASE_URL + "api/transaction/" + transactionId + "/check");
                    HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
                    c.setRequestMethod("GET");
                    c.setReadTimeout(20000);


                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(c.getInputStream(), StandardCharsets.UTF_8))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }

                        return Response.success(new Gson().fromJson(response.toString(), JsonObject.class));
                    }

                }

                @Override
                public void enqueue(Callback callback) {
                    try {
                        callback.onResponse(this, execute());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public boolean isExecuted() {
                    return false;
                }

                @Override
                public void cancel() {

                }

                @Override
                public boolean isCanceled() {
                    return false;
                }

                @Override
                public Call clone() {
                    return null;
                }

                @Override
                public Request request() {
                    return null;
                }
            };
        } else {
            call = serverApi.checkTransaction(token,
                    transactionId);
        }

        return call;
    }

    public Call<JsonObject> declineTransaction(String token, String transactionId) {
        Call call;
        if (Build.VERSION.SDK_INT < 21) {
            call = new Call() {
                @Override
                public Response execute() throws IOException {
                    URL url = new URL(BASE_URL + "api/terminal/transaction/" + transactionId + "/cancel");
                    HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
                    c.setRequestMethod("POST");
                    c.setReadTimeout(20000);
                    c.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    c.setRequestProperty("Token", token);

                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(c.getInputStream(), StandardCharsets.UTF_8))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }

                        return Response.success(new Gson().fromJson(response.toString(), JsonObject.class));
                    }

                }

                @Override
                public void enqueue(Callback callback) {
                    try {
                        callback.onResponse(this, execute());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public boolean isExecuted() {
                    return false;
                }

                @Override
                public void cancel() {

                }

                @Override
                public boolean isCanceled() {
                    return false;
                }

                @Override
                public Call clone() {
                    return null;
                }

                @Override
                public Request request() {
                    return null;
                }
            };
        } else {
            call = serverApi.declineTransaction(token, transactionId);
        }

        return call;
    }
}

