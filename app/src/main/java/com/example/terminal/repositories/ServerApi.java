package com.example.terminal.repositories;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

interface ServerApi {
    @FormUrlEncoded
    @POST("api/user/transaction/create")
    Call<JsonObject> createTransaction(@Header("Token") String userToken, @Field(value = "amount") Long amount);

    @GET("api/transaction/{id}/check")
    Call<JsonObject> checkTransaction(@Header("Token") String userToken, @Path("id") String transactionId);


    @POST("api/terminal/transaction/{id}/cancel")
    Call<JsonObject> declineTransaction(@Header("Token") String userToken, @Path("id") String transactionId);
}
